import sys
import os
import subprocess
import pdb
from pathlib import Path, PureWindowsPath

#assumption: filepath starts with PMD/songs/
def compileM(mml_file,autoexit=True):

	basefilename = mml_file.replace(".mml","")

	#generate .M file directory and removes it if exists already
	#mfile_dir=mml_file.replace("mml","m2")
	mfile_dir=mml_file.replace("mml","m2")
	print(mfile_dir)
	if os.path.isfile(mfile_dir):
		os.remove(mfile_dir)

	cdpath = str(PureWindowsPath(Path(__file__).relative_to(Path.home()).parent / "PMD"))
	pmdcmd = ["dosemu","-dumb",f'"D: || cd {cdpath} || MC\\MCE.EXE /v SONGS\\{basefilename.upper()}.MML"', ">","output.txt"]
	print(pmdcmd)
	with open("script.sh","w") as script:
		print(" ".join(pmdcmd), file=script)

	# #generates directory to cd into PMD directory and specific PMD command
	# cd_dir=os.path.dirname(os.path.realpath(__file__)).replace(os.path.expanduser("~")+"/","").replace("/","\\").upper()+"\\PMD"
	# pmd_command="MC\\MCE.EXE /v/p SONGS\\"+basefilename.upper()+".MML\rexitemu\r"
	# #generates command used with subprocess.run
	# chop=["dosemu","-dumb","-input",repr("D:\rcd "+cd_dir+"\r"+pmd_command).strip("'")]
	# os.environ['TERM'] = 'xterm-256color'
	result = subprocess.run(["sh","script.sh"])
	# result = subprocess.run(pmdcmd,stdout=subprocess.PIPE)
	#extracts and parses result
	with open("output.txt") as f:
		output = f.read()
	start = output.find(".MML file -->",0,len(output))
	end = output.find("\nD:",start,len(output))
	print(output[start:end])

	Path("script.sh").unlink()
	Path("output.txt").unlink()

	return [os.path.isfile(os.path.dirname(os.path.realpath(__file__)) + "/PMD/songs/" + mfile_dir),mfile_dir,output[start:end]]

#extracts wav file from m2 file, courtesy of gzaffin
def gen_flac(filename):
	#puts together filename output
	output = os.path.splitext(filename)[0]+".wav"

	#runs pmdplay
	#pmdplay = subprocess.Popen((os.path.abspath(os.path.dirname(__file__)) + "/pmdplay " + filename + " " + output).split(" "), stdout = subprocess.PIPE,universal_newlines=True).wait()

	result = subprocess.run(["./pmdplay","PMD/songs/" + filename,output], stdout = subprocess.PIPE)

	#export as flac and remove old wav
	# output_flac=os.path.splitext(filename)[0]+".flac"
	# subprocess.run(["sox",output,output_flac])
	# os.remove(output)

	# return output_flac

#this code will (eventually) be broken up into Django space
if __name__== "__main__":
	print(sys.argv)
	response = compileM(sys.argv[1])
	# if response[0]:
		# gen_flac(response[1])