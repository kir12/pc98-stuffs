"""Refitted helper script for arpeggios, now with GUI."""

from dataclasses import dataclass
# import PySimpleGUI as sg
from pathlib import Path
import pdb
import subprocess
from music21 import converter
from music21.articulations import Staccato
from music21.chord import Chord
from music21.note import Rest
from music21.analysis.enharmonics import EnharmonicSimplifier
from music21.interval import Interval
from music21.pitch import Pitch, Accidental
from music21.duration import GraceDuration
from screeninfo import get_monitors
from drum_anal_2 import gen_pc98_duration
import pyperclip


@dataclass
class Params:
    """Struct for requests to PMDBox."""

    inst_index: int
    range: list
    chord_level: int = 1
    key_signature: str = ""
    note_duration: int = 4
    chords_only: bool = False
    voice: int = 1
    exceed_chords: bool = False


# music21 backend
class PMDBox():
    """Work with Music21 to make strings."""

    def __init__(self):
        """Initialize basic state vars."""
        self.filename = ""
        self.music_file = None

        # declare instance vars used in gen_request_state
        self.part = None
        self.output = None
        self.past_note = None
        self.update_elem = None
        self.used_space = None
        self.active_staccato = False
        self.keySignature = None

    def gen_music21_obj(self, new_filename):
        """Attempt to construct music21 object with filename."""
        try:
            self.music_file = converter.parse(new_filename)
            for idx, part in enumerate(self.music_file.parts):
                insts = part.getElementsByClass("Instrument")
                if len(insts) <= 2:
                    transp = insts[0].transposition
                    if transp is not None and transp not in [Interval("P1"), Interval("P8"), Interval("P-8")]:
                        self.music_file.parts[idx].toSoundingPitch(inPlace=True)
            # self.music_file = self.music_file.toSoundingPitch()
        except converter.ConverterException:
            return False
        return True

    def get_insts(self):
        """Get a list of names of the instruments."""
        return [f"{idx+1}: {x.partName}" for idx, x in enumerate(list(self.music_file.parts))]

    # helper functions for processing requests
    def add_space(self):
        """Add a space if not already added."""
        if not self.used_space:
            self.output += " "

    def gen_request_state(self):
        """Reset request state variables."""
        self.part = self.music_file.parts[
            self.args.inst_index-1].getElementsByClass("Measure")
        self.output = ""
        # updates only when another note is passed
        self.past_note = None
        # updates every time an element is passed
        self.update_elem = None
        # duplicate space contigency
        self.used_space = False
        self.active_staccato = False

        # get starting key signature for request domain
        # measure 1 always has key signature, so
        # guaranteed to be defined
        for measure in reversed(self.part[0:self.args.range[0]]):
            if measure.keySignature is not None:
                self.keySignature = measure.keySignature
                break

    def chord_check(self, element):
        """Process opening code logic."""
        # chord check
        if type(element) == Chord:
            try:
                articulations = element.articulations
                element = element[self.args.chord_level-1]
                element.articulations = articulations
            # either skip short chords or use highest one available
            # example: [c,e] and access chord level 3
            except Exception:
                if self.args.exceed_chords:
                    element = Rest(quarterLength=element.quarterLength)
                else:
                    element = element[-1]
        elif self.args.chords_only:
            element = Rest(quarterLength=element.quarterLength)
        return element

    def adjust_pitch(self, element):
        """Manually adjust pitches to simplify number of accidentals."""
        # Pitch(Pitch("C5"), octave=None).ps == Pitch("C").ps
        # adjust pitches to comply with key signature
        # eliminate double sharps/flats if they appear
        if not isinstance(element, Rest):
            new_pitch = element.pitch.simplifyEnharmonic(mostCommon=True)
            # if new pitch is enharmonic spelling of key signature
            for altered_pitch in self.keySignature.alteredPitches:
                if Pitch(new_pitch, octave=None).ps == altered_pitch.ps:
                    # get starting keys of each pitch
                    natural_new_pitch = Pitch(new_pitch, accidental=Accidental("natural"), octave=None)
                    natural_altered_pitch = Pitch(altered_pitch, accidental=Accidental("natural"), octave=None)

                    # get reference b and c
                    natural_b = Pitch("B", accidental=Accidental("natural"))
                    natural_c = Pitch("C", accidental=Accidental("natural"))

                    # set to spelling of key signature
                    new_pitch = Pitch(altered_pitch, octave=new_pitch.octave)

                    # adjust octave if needed
                    if natural_new_pitch == natural_b and natural_altered_pitch == natural_c:
                        new_pitch.octave += 1
                    elif natural_new_pitch == natural_c and natural_altered_pitch == natural_b:
                        new_pitch.octave -= 1
                    break
            element.pitch = new_pitch
        return element

    def start_staccato_check(self, element):
        """Process stacattos at the start of a new note."""
        # case 1: staccato detected
        if Staccato in map(lambda x: type(x), element.articulations):
            # case 1.5: first instance of staccato
            if not self.active_staccato:
                self.active_staccato = True
                self.add_space()
                self.output += "Q4 "
                self.used_space = True
        # case 2: first non staccato detected
        elif self.active_staccato:
            self.active_staccato = False
            self.add_space()
            self.output += "Q8 "
            self.used_space = True

    def octave_check(self, element):
        """Process octave differences, if there is one."""
        # octave check
        if self.past_note is not None and type(element) != Rest:
            oc_diff = element.octave - self.past_note.octave
            comp = ">" if oc_diff > 0 else "<"
            oc_diff = abs(oc_diff)
            if oc_diff > 0:
                self.add_space()
                self.output += f"{comp * oc_diff} "
                self.used_space = True

    def process_note(self, idx, element):
        """Process note, duration, and accidentals."""
        # get note name
        note = element.name.replace("#", "").replace("-", "").lower().replace("rest", "r")

        # get signature
        in_key = note in self.args.key_signature
        if in_key:
            if "#" not in element.name and "-" not in element.name:
                acc = "="
            else:
                acc = ""
        elif "#" in element.name:
            acc = "+"
        elif "-" in element.name:
            acc = "-"
        else:
            acc = ""

        # compute duration and add note
        duration = gen_pc98_duration(element.quarterLength)
        if idx == 0 or self.update_elem is None:
            past_duration = duration
        else:
            past_duration = gen_pc98_duration(self.update_elem.quarterLength)

        # special case: note is continuing or ending a tie
        if element.tie is not None and element.tie.type in ["continue", "stop"]:
            self.output += f"&{duration}"
        else:  # regular case
            # if duration doesn't match previous note, must add space
            # if not already added
            if duration != past_duration and not self.used_space:
                self.output += " "

            # write actual note
            self.output += f"{note}{acc}"

            # if a note doesn't match common duration set by user,
            # must be manually set
            if duration != str(self.args.note_duration):
                self.output += f"{duration}"

        # update used space
        self.used_space = False

    def process_request(self, args):
        """Generate PMD for particular request."""
        # check measure ranges
        if (
            any(num < 1 for num in args.range) or
            args.range[0] > args.range[1]
        ):
            return "Error: The stated measure range is invalid"
        try:
            # initialize state vars
            self.args = args
            self.gen_request_state()
            for measure in self.part[self.args.range[0]-1: self.args.range[1]]:
                # extract proper voice if needed
                if len(measure.voices) > 0:
                    measure = measure.voices[args.voice-1]
                for idx, element in enumerate(measure.notesAndRests):
                    
                    # check for key signature change
                    if measure.keySignature is not None:
                        self.keySignature = measure.keySignature

                    # ignoring grace notes
                    if element.duration.__class__ == GraceDuration:
                        continue

                    # process chords
                    element = self.chord_check(element)

                    # get simplified element compliant with key signature
                    element = self.adjust_pitch(element)

                    # handle octave and note itself
                    self.octave_check(element)

                    # check for stacco and process
                    self.start_staccato_check(element)

                    self.process_note(idx, element)

                    # update past element
                    if type(element) != Rest:
                        self.past_note = element
                    self.update_elem = element
                
                # end active staccato at end of measure
                if self.active_staccato:
                    self.add_space()
                    self.output += "Q8 "
                    self.used_space = True
                    self.active_staccato = False
                if not self.used_space:
                    self.output += " "
                    self.used_space = True
            return self.output
        except Exception as e:
            raise e
            return "Error: an unexpected problem has occured"

# class WBar():
#     """Frontend to interact with user."""
# 
#     def get_location(self):
#         """Get center of primary monitor."""
#         location = (0, 0)
#         for monitor in get_monitors():
#             if monitor.is_primary:
#                 m_spec = (monitor.width/2, monitor.height/2)
#                 location = tuple(map(sum, zip(location, m_spec)))
#                 break
#             else:
#                 m_spec = (monitor.width, 0)
#                 location = tuple(map(sum, zip(location, m_spec)))
#         return location
# 
#     def event_loop(self):
#         """Process and handle events."""
#         while True:
# 
#             event, values = self.window.read()
#             if values is not None:
#                 new_filename = values.get("FILEPATH", "")
#                 if new_filename != self.filename:
#                     self.get_new_file(new_filename)
#             if event in [sg.WIN_CLOSED, "Cancel"]:
#                 break
#             elif event == "SUBMIT":
#                 self.handle_submit(values)
#             elif event == "COPY":
#                 self.handle_copy(values)
# 
#         self.window.close()
# 
#     def __init__(self):
#         """Set up PySimpleGui properties."""
#         # simplegui properties
#         sg.theme('Dark Blue 3')
#         self.layout = [
#             [sg.Text('Input')],
#             [sg.Input(
#                 enable_events=True,
#                 readonly=True, key="FILEPATH"),
#                 sg.FileBrowse(file_types=(
#                     ("Musescore", "*.mscz"),
#                     ("MIDI", "*.mid"),
#                     ("MIDI", "*.midi"),
#                     ("MusicXML", "*.mxl"),
#                     ("ALL Files", ". *")
#                 ))],
#             [sg.Text("Instrument Select"),
#                 sg.Combo(
#                     [],
#                     readonly=True,
#                     disabled=True,
#                     expand_x=True,
#                     key="INSTRUMENTS")],
#             [sg.Text("Measure Start"),
#                 sg.Input(size=3, disabled=True, key="M_START"),
#                 sg.Text("Measure End"),
#                 sg.Input(size=3, disabled=True, key="M_END"),
#                 sg.Text("Voice Select"),
#                 sg.Input(size=3, disabled=True, key="VOICE")],
#             [sg.Text("Key Signature"),
#                 sg.Input(size=8, disabled=True, key="KEY_SIGN"),
#                 sg.Text("Default Note Duration"),
#                 sg.Input(size=3, disabled=True, key="DURATION")],
#             [sg.Text("Chord Level"),
#                 sg.Input(size=3, disabled=True, key="CHORD"),
#                 sg.Text("Chords Only"),
#                 sg.Checkbox("", disabled=True, key="CHORDS_ONLY"),
#                 sg.Text("Ignore Exceeding Chords"),
#                 sg.Checkbox("", disabled=True, key="EXCEED_CHORDS")],
#             [sg.Submit(disabled=True, key="SUBMIT"), sg.Cancel()],
#             [sg.Text("Output")],
#             [sg.Multiline(
#                 enable_events=False,
#                 disabled=True,
#                 auto_size_text=True,
#                 expand_y=True,
#                 size=(50, 5),
#                 key="OUTPUT")],
#             [sg.Button("Copy", disabled=True, key="COPY")]
#         ]
# 
#         self.window = sg.Window(
#             'PMD Arpeggio Destroyer', self.layout,
#             location=self.get_location(),
#             icon='icon.png'
#             )
# 
#         # WBar state vars
#         self.filename = ""
# 
#         self.pmdbox = PMDBox()
# 
#     def handle_submit(self, values):
#         """Recieve new event, parse and send to back end."""
#         try:
#             params = Params(
#                 inst_index=int(values['INSTRUMENTS'].split(": ")[0]),
#                 range=[int(values['M_START']), int(values['M_END'])],
#                 key_signature=values['KEY_SIGN'],
#                 chords_only=values['CHORDS_ONLY'],
#                 exceed_chords=values['EXCEED_CHORDS']
#             )
#             if values['CHORD'] != '':
#                 params.chord_level = int(values['CHORD'])
#             if values['DURATION'] != '':
#                 params.note_duration = int(values['DURATION'])
#             if values['VOICE'] != '':
#                 params.voice = int(values['VOICE'])
# 
#             self.update_output(self.pmdbox.process_request(params))
#         except ValueError:
#             self.update_output("Error: An invalid parameter was supplied")
# 
#     def get_new_file(self, new_filename):
#         """Process a new file."""
#         self.update_output("Processing file, please wait...")
# 
#         new_f_obj = Path(new_filename).resolve()
#         # reject invalid files
#         if not new_f_obj.is_file():
#             self.update_output("Error: An invalid input file was supplied.")
#             self.disable_form()
#             return
# 
#         # convert to musicxml if need be
#         if new_f_obj.suffix not in [".mxl"]:
#             self.update_output("Converting to MusicXML...")
#             subprocess.run(
#                 ["mscore", "-o", new_f_obj.with_suffix('.mxl'), new_filename],
#                 stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL
#             )
#             if not new_f_obj.with_suffix('.mxl').is_file():
#                 self.update_output("Error: Something went wrong with converting to MusicXML.")
#                 self.disable_form()
#                 return
# 
#         # open in music21
#         self.update_output("Opening in music21...")
#         if not self.pmdbox.gen_music21_obj(new_f_obj.with_suffix(".mxl")):
#             self.update_output("Error: Something went wrong with opening in Music21.")
#             self.disable_form()
#             return
# 
#         # populate rest of the fields
#         self.update_inst_list(self.pmdbox.get_insts(), False)
#         self.update_other_params(False)
#         self.update_submit(False)
#         self.update_output("")
# 
#         self.filename = new_filename
# 
#     def disable_form(self):
#         """Disable the entire form."""
#         self.update_inst_list([], True)
#         self.update_other_params(True)
#         self.update_submit(True)
# 
#     def update_output(self, text):
#         """Change what appears in the output box."""
#         self.window.Element("OUTPUT").update(text)
#         self.window.refresh()
# 
#     def update_inst_list(self, values, disabled):
#         """Change value and condition of instrument list."""
#         self.window.Element("INSTRUMENTS").update(values=values, disabled=disabled)
#         self.window.refresh()
# 
#     def update_other_params(self, disabled):
#         """Enable or disable other numerical params."""
#         self.window.Element("M_START").update(disabled=disabled)
#         self.window.Element("M_END").update(disabled=disabled)
#         self.window.Element("KEY_SIGN").update(disabled=disabled)
#         self.window.Element("DURATION").update(disabled=disabled)
#         self.window.Element("CHORD").update(disabled=disabled)
#         self.window.Element("CHORDS_ONLY").update(disabled=disabled)
#         self.window.Element("COPY").update(disabled=disabled)
#         self.window.Element("VOICE").update(disabled=disabled)
#         self.window.Element("EXCEED_CHORDS").update(disabled=disabled)
#         self.window.refresh()
# 
#     def update_submit(self, disabled):
#         """Update button status."""
#         self.window.Element("SUBMIT").update(disabled=disabled)
#         self.window.refresh()
# 
#     def handle_copy(self, values):
#         pyperclip.copy(values["OUTPUT"])
# 
# if __name__ == "__main__":
#     WBar().event_loop()
