# pc98-arranges

## Brief Intro

This is my project repository for making song arrangements for the PC-98 platform. 
For those who don't know, the PC-98 was a series of 16-bit Japanese computers used the YM2608 -- a sound card with a great sound palette which powered a lot of 16-bit game OSTs.
These days, PC-98 styled arrangements exist for lots of popular video game music; this repository is one such example.

As an example, here's one PC-98 arrange that I made recently: [*Lonesome Werewolf*](https://www.youtube.com/watch?v=YkptIAEVKn4) coming from the video game "Double Dealing Character."

## Detailed Workflow

My project workflow is as follows:
- Retrieve an existing MIDI arrange of the song I'm doing
- While consulting the MIDI arrange and the original song, create a working draft of the arrange inside a `.mml` file 
- Compile it using M. Kajihara's [Professional Music Driver (PMD)](https://battleofthebits.org/lyceum/View/Professional+Music+Driver+%28PMD%29/)
- Prototype the output `.M2` file using FMPMD2000, then record it over 98fmplayer.

I also wrote two Python files to automate the more tedious parts of the development process:
- `msdos.py`: This script automatically calls the `dosemu` MS-DOS emulator, runs PMD, and runs `pmdmini` to convert to `.wav`.
This approach is a little overkill, but unfortunately `dosemu` is the only Linux emulator which has *any* scripting support, and requires a lot of gobbledygook to point it to the proper directory.
On the bright side, I can now get immediate compiling by simply calling `make`, which is a huge development timesaver.

- `drum_anal_2.py`: This script, when given a MIDI or MusicXML source file, (a) automatically generates the percussion portion of my PC-98 arranges and (b) detects when drum patterns are repeated and flags it.
The normal process of entering percussion notes is simple but very tedious -- the ideal scenario for script automation.
However, `music21` (the standard music analysis tool) has no drumset support.
My workaround was to manually parse the source file (which is MusicXML) using Python's stock `ElementTree` XML parser, and combine it with note duration data from `music21`.

The remaining Python/C++ files were all earlier versions of `drum_anal_2.py`.

## Repository File Guide

My MIDI source files and MML files are organized in `library/`. (the latter of which is via relative symbolic link)
If you want to view them all for some reason, they're accessible in `PMD/songs/`.
PMD and FMPMD2000 are also inside the `PMD/` general folder.

## Development Guidelines

Recording levels between FMPMD2000 and Neko Project are a bit janky, in that different channels are louder on either one of the platforms. 
To that end, below are some principles I've developed through experimentation to try and standardize them.

- Neko Project: set volume to 88% to standardize SSG. (Do this by scrolling down in the options bar)
- FMPMD2000:
    - PPZ8: set volume down to -14
    - SSG: do not change
    - FM: set volume down to -5
    - I don't currently use FM3EX, Rhythm, and ADPCM but will adjust them too if needed