'''
parameters:
0) type of operation (condense, explode into multiple instruments)
1) index of instrument (mandatory, int)
2) start measure (mandatory, int)
3) end measure (mandatory, int)
4) path to filename
'''

from music21 import converter
from music21.chord import Chord
from music21.note import Rest
from music21.articulations import Staccato
import argparse
import subprocess
from pathlib import Path
import pdb
from drum_anal_2 import gen_pc98_duration

ERRORS = {
    'INPUT': "Error: An invalid input file was supplied. (Make sure the pathname is relative to this script!",
    'INST_RANGE': "Error: the instrument number is out of range",
    'MEAS_RANGE': "Error: The stated measure range is invalid",
    "ERR_PMD": "Error: invalid parse-pmd CMD input"
}


# general function for paramter validation
def test_err(err_bool, err_type):
    if err_bool:
        print(ERRORS[err_type])
        exit(1)


def process_args():

    # define args and parse them
    parser = argparse.ArgumentParser(
        description="Useful MIDI processing operations for PMD",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    # add initial file parameters
    input_group = parser.add_argument_group(
        'input arguments', 'Parameters for input MIDI file')
    input_group.add_argument(
        '-i', '--input', required=True, type=str,
        help='Source music file (MIDI, MXL, or MSCZ)'
    )
    input_group.add_argument(
        '-idx', '--inst-index', required=True, type=int,
        metavar=('NUM'),
        help='Index of instrument to operate on (start from 1!)')
    input_group.add_argument(
        '-r', '--range', nargs=2, type=int, required=True,
        metavar=('START', 'END'),
        help='Range of measures to operate on (start from 1!)')

    # declare subfunctions
    subparsers = parser.add_subparsers(
        dest="operation", required=True,
        help="Type of PMD operation",)

    # define commands for parsing PMD
    parse_pmd = subparsers.add_parser(
        "parse-pmd", help="Convert measures into PMD code")
    parse_pmd.add_argument(
        "-k", "--key-signature", type=str,
        help="Initial key signature of measure", default="")
    parse_pmd.add_argument(
        "-d", "--note-duration", type=int,
        help="Default note duration", default=4)
    parse_pmd.add_argument(
        '-c', '--chord-level', type=int, default=1,
        help="Level of note to select if chords encountered"
    )

    # define commands for condensing
    c_subp = subparsers.add_parser(
        'condense',
        help='Destructively combine all voices in measures')
    c_subp.add_argument(
        '-o', '--output', type=str, default=None,
        help='Specify relative path to output .mxl file')

    # define commands for exploding
    e_subp = subparsers.add_parser(
        'explode',
        help='Detructively explode voices into multiple instruments')
    e_subp.add_argument(
        '-o', '--output', type=str, default=None,
        help='Specify relative path to output .mxl file')

    # extract arguments
    args = parser.parse_args()

    # args post-processing (that doesn't use Music21 data)
    f_obj = Path(args.input).resolve()
    test_err(
        not f_obj.is_file() or
        f_obj.suffix not in ['.mscz', '.mid', '.mxl', '.midi'], 'INPUT')
    test_err(args.inst_index < 1, 'INST_RANGE')
    test_err(any(num < 1 for num in args.range), 'MEAS_RANGE')
    test_err(args.range[0] > args.range[1], 'MEAS_RANGE')

    # convert midi/mscz files to mxl (to ensure music21 can read w. measures)
    if f_obj.suffix != ".mxl":
        print("Converting MIDI/MuseScore input to MusicXML...")
        subprocess.run(
            ["mscore", "-o", f_obj.with_suffix('.mxl'), args.input],
            stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL
        )
        args.input = f_obj.with_suffix(".mxl")

    # # fill default output value
    # if args.output is None:
    #     args.output = f'{first_chunk}_OUTPUT.mxl'
    # # issue warning if overwriting
    # elif args.output == args.input:
    #     print('Warning: output file will overwrite input file')

    return args


def parse_pmd(music_file, args):
    try:
        part = music_file.parts[args.inst_index-1].getElementsByClass("Measure")
        output = ""
        past_note = None  # updates only when another note is passed
        update_elem = None  # updates every time an element is passed
        used_space = False  # duplicate space contigency
        active_staccato = False
        for measure in part[args.range[0]-1: args.range[1]]:
            for idx, element in enumerate(measure.notesAndRests()):

                # case 1: staccato detected
                if Staccato in map(lambda x: type(x), element.articulations):
                    # case 1.5: first instance of staccato
                    if not active_staccato:
                        active_staccato = True
                        if not used_space:
                            output += " "
                        output += "Q4 "
                        used_space = True
                # case 2: first non staccato detected
                elif active_staccato:
                    active_staccato = False
                    if not used_space:
                        output += " "
                    output += "Q8 "
                    used_space = True

                # chord check
                if type(element) == Chord:
                    element = element[args.chord_level-1]

                # octave check
                if past_note is not None and type(element) != Rest:
                    oc_diff = element.octave - past_note.octave
                    comp = ">" if oc_diff > 0 else "<"
                    oc_diff = abs(oc_diff)
                    if oc_diff > 0:
                        if not used_space:
                            output += " "
                        output += f"{comp * oc_diff} "
                        used_space = True

                # get note name
                note = element.name.replace("#", "").replace("-", "").lower().replace("rest","r")

                # get signature
                in_key = note in args.key_signature
                if in_key:
                    if "#" not in element.name and "-" not in element.name:
                        acc = "="
                    else:
                        acc = ""
                elif "#" in element.name: 
                    acc = "+"
                elif "-" in element.name:
                    acc = "-"
                else:
                    acc = ""

                # compute duration and add note
                duration = gen_pc98_duration(element.quarterLength)
                if idx == 0:
                    past_duration = duration
                else:
                    past_duration = gen_pc98_duration(update_elem.quarterLength)

                # if duration doesn't match previous note, must add space
                # if not already added
                if duration != past_duration and not used_space:
                    output += " "

                # write actual note
                output += f"{note}{acc}"

                # if a note doesn't match common duration set by user,
                # must be manually set
                if duration != str(args.note_duration):
                    output += f"{duration}"

                # update used space
                used_space = False

                # update past element
                if type(element) != Rest:
                    past_note = element
                update_elem = element

            if active_staccato:
                if not used_space:
                    output += " "
                output += "Q8 "
                used_space = True
                active_staccato = False
            if not used_space:
                output += " "
                used_space = True
        return output
                
    except Exception as e:
        raise e


if __name__ == "__main__":

    # initial processing in music21
    args = process_args()
    try:
        print("Opening file in music21...")
        music_file = converter.parse(args.input)
    except converter.ConverterException:
        test_err(True, 'INPUT')

    if args.operation == "parse-pmd":
        test_err(args.note_duration < 1, "ERR_PMD")
        print(parse_pmd(music_file, args))
    elif args.operation == 'condense':
        pass
    else:  # explode
        pass

    exit(0)
