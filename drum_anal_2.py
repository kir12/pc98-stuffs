from music21 import converter, duration
import xml.etree.ElementTree as ET
import zipfile
import os
import math
import subprocess
import argparse
from copy import deepcopy
from music21.percussion import PercussionChord
from music21.chord import Chord

drum_trans = {
    "I1": 512,  # chinese cymbal? 
    "I2": 256,  # open high hat apparently
    "I3": 512,  # chinese cymbal?
    "I17": 16,
    "I28": 32,
    "I29": 1,
    "I32": 32,  # sounds like sidestick
    "I34": 32,  # sounds like sidestick
    "I35": 32,  # unknown, sounds kinda like sidestick
    "I36": 1,  # Acoustic Bass Drum",
    "I37": 1,  # "Bass Drum 1",
    "I38": 32,  # "Side Stick",
    "I39": 2,  # "Acoustic Snare",
    "I40": 66,  # "Instrument 40", (both snare drums)
    "I41": 64,  # "Electric Snare",
    "I42": 4,  # "Low Floor Tom",
    "I43": 128,  # "Closed Hi-Hat",
    "I44": 16,  # "High Floor Tom"
    "I45": 128,  # "Pedal Hi-Hat"
    "I46": 4,  # "Low Tom",
    "I47": 256,  # "Open Hi-Hat",
    "I48": 8,  # "Low-Mid Tom",
    "I49": 8,  # "Hi-Mid Tom",
    "I50": 512,  # "Crash Cymbal 1",
    "I51": 16,  # "High Tom",
    "I52": 1024,  # "Ride Cymbal 1",
    "I53": 512,  # "Chinese Cymbal",
    "I54": 1024,
    "I55": 1024,  # "Tambourine",
    "I56": 1536,  # "Splash Cymbal",
    "I57": "Cowbell",
    "I58": 512,  # Crash Cymbal 2",
    "I60": 1024,  # "Ride Cymbal 2",
    "I61": 8,
    "I62": 512,
    "I63": 32,  # mute high tonga?
    "I64": 16,  # "Open Hi Conga",
    "I65": 4,  # "Low Conga",
    "I68": 32,  # sounds kinda like side stick?
    "I70": 32,
    "I71": 16,
    "I76": 4,
    "I77": 16,  # probably high conga
    "I81": 32,  # triangle open (also sounds kinda like rimshot)
    "I82": 64,  # no official definition (???)
    "I85": 8,  # no def
    "I87": 8,
    "I99": 1024,  # no official definition, next to I81 in source
    "I106": 32,
    "I107": 2,
    "I109": 64, 
    "I101": 16,   # also no official definition
    "I103": 64,
    "I110": 8,  # no official definition
    "I111": 256,  # also no offiical definition
    "I112": 64,
    "I114": 8,
    "I115": 16,
    "I123": 32,
}


# given an array of part indices, extracts them via old-school ElementTree
# NOTE: measure extraction does NOT happen here
def extract_xml_parts(file_dir):

    # extract xml file from mxl file (treat as zip)
    archive = zipfile.ZipFile(file_dir, 'r')
    xml_dir = os.path.splitext(os.path.basename(file_dir))[0]+".xml"
    xml_file = archive.open(xml_dir)

    # open file in ElementTree, extract specific parts
    tree = ET.parse(xml_file)
    root = tree.getroot()

    # perform our own search for drum instruments
    # reason: Music21 and ElementTree represent multiple-staffed
    # instruments differently.
    part_lists = root.findall("./part-list/score-part")
    drum_index = []
    for i in range(len(part_lists)):
        # add part if more than one inst or part has unpitched inst
        if len(part_lists[i].findall("./score-instrument")) > 1 or \
                part_lists[i].find("./midi-instrument").find("./midi-unpitched") is not None:
            drum_index.append(i)

    return [root.findall("./part")[i] for i in drum_index]


# extracts parts via music21, and calls xml method
def check_for_drums(file_dir):
    output = {"music21": [], "xml": []}
    drum_part_idx = []

    # initialize key music21 assets
    print("Opening MusicXML file in music21... ", end="", flush=True)
    score = converter.parse(file_dir)
    print("Done!")
    for i in range(len(score.parts)):
        part = score.parts[i]
        m_part = part.getElementsByClass("Measure")

        # if percussion clef, add part and index
        if any(obj.__class__.__name__ == "PercussionClef"
                for obj in m_part[0]):
            output["music21"].append(m_part)
            drum_part_idx.append(i)

    # grab xml part data
    output["xml"] = extract_xml_parts(file_dir)
    return output


# check if terminating (a.k.a. triplet)
# source: https://stackoverflow.com/questions/14481054/is-there-a-way-to-tell-if-a-decimal-is-terminating-or-repeating-in-c
def isTerminating(numerator, denominator):
    while denominator % 2 == 0:
        denominator /= 2
    while denominator % 5 == 0:
        denominator /= 5
    return denominator == 1


# given a duration where 1.0=quarter note,
# calculate/return PC-98 respective melody
def gen_pc98_duration(duration):
    def pc98(x): return (1.0/x)*4.0
    output = pc98(duration)
    # most cases: will fall evenly under a regular note
    if abs(round(output) - output) < 0.00001:
        return str(round(output))

    # special case: triplets
    # dump into dictionary
    frac = duration.as_integer_ratio()
    if not isTerminating(frac[0], frac[1]):
        raise AttributeError("fix your shit")
        # some return here

    # final case: dotted notes of some kind
    # idea: lop off notes that cleanly fall under quarter system
    # until we get to zero
    output_chk = duration
    duration_chain = []
    while output_chk > 0:
        subportion = 2 ** math.floor(math.log2(output_chk))
        duration_chain.append(subportion)
        output_chk -= subportion
    return "&".join(str(int(pc98(note))) for note in duration_chain)


# given a set of measures, generates a PMD compatible pattern
# TODO: separate music21 and xml into voices
def new_pattern(measures, flag_unknown_instruments):
    # further strain out measures to individual voices
    # at this point, it is just notes and rests
    voices = {
        "music21": [],
        "xml": []
    }

    # helper method for reducing chords to standard Notes
    def process_m21(nr_data):
        i = 0
        while i < len(nr_data):
            cname = nr_data[i].__class__
            # expand chords if needed
            if cname in [PercussionChord, Chord]:
                expand_chord = list(nr_data[i])
                for note in expand_chord:
                    note.offset = nr_data[i].offset
                nr_data = nr_data[0:i] + expand_chord + nr_data[i+1:]
                i -= 1
            # tremelo check
            elif len(nr_data[i].expressions) > 0:
                # extract tremelos (should be only one)
                tremelo_lst = [cl for cl in nr_data[i].expressions if cl.__class__.__name__ == 'Tremolo']
                if len(tremelo_lst) > 0:
                    # determine tremelo type and duration of new notes
                    tr_num = tremelo_lst[0].numberOfMarks
                    note_num = 2**tr_num
                    new_duration = duration.Duration(quarterLength=nr_data[i].duration.quarterLength/note_num)
                    expand_tremelo = []
                    # construct notes and apply to nr data
                    for a in range(note_num):
                        new_note = deepcopy(nr_data[i])
                        new_note.duration = new_duration
                        new_note.expressions = []
                        new_note.offset = nr_data[i].offset + (a*new_duration.quarterLength)
                        expand_tremelo.append(new_note)
                    nr_data = nr_data[0:i] + expand_tremelo + nr_data[i+1:]
                    i -= 1
            i += 1

        return nr_data

    # strain out music21
    for measure in measures["music21"]:
        if len(list(measure.voices)) > 0:
            voices["music21"] += [process_m21(list(voice.notesAndRests))
                                  for voice in list(measure.voices)]
        else:
            voices["music21"] += [process_m21(list(measure.notesAndRests))]

    # strain out musicxml (maybe clean out later?)
    for idx, measure in enumerate(measures["xml"]):
        for i in range(1, 5):
            temp_voice = measure.findall("note[voice='" + str(i) + "']")
            # if notes exist, then convert them to PC-98 notes
            if len(temp_voice) != 0:
                inst_rep = []
                for note in temp_voice:
                    inst_search = note.find("instrument")
                    # inst --> PC-98 (using instrument attribute)
                    if inst_search is not None:
                        inst_id = inst_search.attrib["id"]
                        # raise exception if unknown inst found
                        if flag_unknown_instruments:
                            try:
                                inst_num = drum_trans[inst_id.split("-")[1]]
                            except:
                                raise AttributeError(f"Bad Instrument {inst_id} at Voice {i} at measure {measure.attrib['number']}")
                            if type(inst_num) == str:
                                raise AttributeError(f"Unknown Instrument: {inst_id}")
                        else:  # otherwise, proceed normally
                            inst_num = drum_trans.get(
                                inst_id.split("-")[1], 0)
                            if type(inst_num) == str:
                                inst_num = 0
                        # add notes as necessary for tremolo
                        tremelo_check = note.find(".//tremolo")
                        if tremelo_check is not None:
                            num_notes = 2**int(tremelo_check.text)
                        else:
                            num_notes = 1
                        for a in range(num_notes):
                            inst_rep.append(inst_num)
                    # inst=0 is placeholder for rest
                    else:
                        inst_rep.append(0)
                voices["xml"].append(inst_rep)

    # merge musicxml instrument data with music21 note data
    merged_results = []
    for note_voice, inst_voice in zip(voices["music21"], voices["xml"]):
        for note, inst in zip(note_voice, inst_voice):
            merged_results.append((note, inst))

    # apply sort based on music21 offset
    total_length = measures["music21"][0].duration.quarterLength
    merged_results.sort(key=lambda note: note[0].offset)

    # actual PC-98 writing happens here
    output = ""
    note_idx = 0
    while note_idx < len(merged_results):
        base_offset = merged_results[note_idx][0].offset
        # set of all instruments that play at time base_offset
        inst_collec = {merged_results[note_idx][1]}
        next_idx = note_idx+1  # keep track of next note(s) that play
        # if the next note plays at same time:
        while next_idx < len(merged_results) and \
                base_offset == merged_results[next_idx][0].offset:
            # add inst to set, increment next_idx
            inst_collec.add(merged_results[next_idx][1])
            next_idx += 1

        # special case: next note is very end
        if next_idx >= len(merged_results):
            true_duration = total_length - base_offset
        # default case: subtract this note offset from next note offset
        else:
            true_duration = merged_results[next_idx][0].offset - base_offset
        # put together and write pc98 note to output
        pc98_duration = gen_pc98_duration(true_duration)
        inst_sum = sum(inst_collec)
        output += "r" if inst_sum == 0 else "@"+str(inst_sum) + "c"
        output += str(pc98_duration) + " "
        note_idx = next_idx

    return output


# master function for parsing measures
def parse_parts(drum_parts, w_file, flag_unknown_instruments):

    # reduce all drum parts to just measures
    drum_parts = {
        "music21": [list(part) for part in drum_parts["music21"]],
        "xml": [part.findall("measure") for part in drum_parts["xml"]]
    }

    r_channel = 0
    m_library = []
    f = open(w_file, "w")
    # fundamental assumption: all parts have equal measures
    for m_idx in range(len(drum_parts["music21"][0])):

        # extract a single column of measures, and then generate pattern for it
        measures = {
            "music21": [part_m[m_idx] for part_m in drum_parts["music21"]],
            "xml": [part_m[m_idx] for part_m in drum_parts["xml"]]
        }
        pmd_code = new_pattern(measures, flag_unknown_instruments)
        if pmd_code not in m_library:
            print("R"+str(r_channel)+" "+pmd_code, file=f)
            m_library.append(pmd_code)
            r_channel += 1
        else:
            print(
                "; Measure " + str(m_idx+1) +
                " found at R"+str(m_library.index(pmd_code)), file=f)


if __name__ == "__main__":

    # construct parsing objects
    parser = argparse.ArgumentParser(
        description="Automatic generation of PMD percussion code using MIDI source files.")
    parser.add_argument(
        "input_file", type=str,
        help="Must be a MIDI or MuseScore file")
    parser.add_argument(
        "-flag_inst", "--flag_unknown_instruments",
        action="store_true", default=False,
        help="Flag unrecognized drum instruments via Exception")
    parser.add_argument(
        "-o", "--output", type=str, default=None,
        help="Specify relative path to custom output file for PMD code")
    parser.add_argument(
        "-k", "--keep-mxl-file", action="store_true", default=False,
        help="Keep converted .xml file after program is done"
    )
    args = parser.parse_args()

    # do file validation
    f_split = os.path.splitext(args.input_file)
    if f_split[1] in [".mid", ".midi", ".mscz"]:
        print("Converting MIDI/MuseScore input to MusicXML...")
        subprocess.run(
            ["mscore", "-o", f_split[0] + ".mxl", args.input_file],
            stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL
        )
    if not os.path.isfile(f_split[0]+".mxl"):
        raise AttributeError("An invalid music file type was submitted!")

    # proceed once file validation completed
    drum_parts = check_for_drums(f_split[0]+".mxl")
    if len(drum_parts) == 0:
        raise AttributeError("No drum parts were found in your file.")

    # construct default output file if needed, then pass to next method
    if args.output is None:
        args.output = f_split[0] + ".txt"
    print("Now writing to output " + args.output)
    parse_parts(drum_parts,
                os.path.join(os.path.dirname(__file__), args.output),
                args.flag_unknown_instruments)
    if not args.keep_mxl_file:
        print("Removing generated mxl file")
        os.remove(f_split[0]+".mxl")
