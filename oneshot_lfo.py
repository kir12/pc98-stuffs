import argparse
import math

if __name__ == "__main__":
    # construct parsing objects
    parser = argparse.ArgumentParser(
        description="Automatic generation of LFO patterns given a set of parameters.")
    parser.add_argument(
        "length", type=int,
        help="Length of measure where LFO applies")
    parser.add_argument(
        "range", type=int,
        help="Preferred volume range"
    )
    parser.add_argument(
        "loud", type=str,
        help="True/False: is LFO crescendo?"
    )
    args = parser.parse_args()

    factors = [n for n in range(1, args.length+ 1) if args.length % n == 0]
    pivot = math.ceil(len(factors)/2)
    factor_pairs = [(factors[n], factors[-1-n]) for n in range(pivot)]
    for pair in factor_pairs:
        speed = min(pair)
        depthB = max(pair)
        depthA = 1
        while depthA * depthB < args.range:
            depthA += 1
        
        print(f"MWB6 MB0,{speed},{depthA},{depthB} *B6")
