from music21 import converter
score = converter.parse("/home/brianl/Documents/Untitled-2.mxl")

# original measure to compare
print("# original score")
print(score.parts[0].getElementsByClass("Measure")[0].show("text"))

# try 1: chordify the entire score
print("# try 1")
test01 = score.chordify()
print(test01.getElementsByClass("Measure")[0].show("text"))

# try 2: chordify just the part
print("# try 2")
test02 = score.parts[0].chordify()
print(test02.getElementsByClass("Measure")[0].show("text"))

print("# try 3")
# try 3: chordify just the measure
test03 = score.parts[0].getElementsByClass("Measure")[0].chordify()
print(test03.show("text"))