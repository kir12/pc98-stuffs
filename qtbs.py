import sys
from PySide6.QtCore import Slot, Signal
from PySide6.QtWidgets import QApplication, QLabel, QDialog, QBoxLayout, QFileDialog, QLineEdit, QWidget, QPushButton, QPlainTextEdit, QComboBox, QCheckBox
from PySide6.QtGui import QIcon, QTextOption
from frontend import PMDBox, Params
import time

class InstrumentOptions(QWidget):

    instrumentListSignal = Signal(list)

    def __init__(self,parent=None):
        super(InstrumentOptions, self).__init__(parent)
        self.layout = QBoxLayout(QBoxLayout.Direction.LeftToRight)

        self.instLabel = QLabel("Instrument Select")
        self.instSelect = QComboBox(editable=False, insertPolicy = QComboBox.NoInsert)
        self.instSelect.setEnabled(False)

        self.layout.addWidget(self.instLabel)
        self.layout.addWidget(self.instSelect)
        self.setLayout(self.layout)

        self.instrumentListSignal.connect(self.change_instrument_options)

    @Slot()
    def change_instrument_options(self, insts):
        if len(insts) > 0:
            self.instSelect.addItems(insts)
            self.instSelect.setEnabled(True)
        else:
            self.instSelect.setEnabled(False)
            self.instSelect.clear()
    
    def get_selected_index(self):
        return self.instSelect.currentIndex() +1

class MeasureOptions(QWidget):
    
    measureSignal = Signal(bool)

    def __init__(self,parent=None):
        super(MeasureOptions, self).__init__(parent)
        self.layout = QBoxLayout(QBoxLayout.Direction.LeftToRight)

        self.mstartlabel = QLabel("Measure Start")
        self.mstart = QLineEdit(readOnly=True)
        self.mendlabel = QLabel("Measure End")
        self.mend = QLineEdit(readOnly=True)
        self.voicelabel = QLabel("Voice Select")
        self.voice = QLineEdit(readOnly=True)

        self.layout.addWidget(self.mstartlabel)
        self.layout.addWidget(self.mstart)
        self.layout.addWidget(self.mendlabel)
        self.layout.addWidget(self.mend)
        self.layout.addWidget(self.voicelabel)
        self.layout.addWidget(self.voice)
        self.setLayout(self.layout)

        self.measureSignal.connect(self.change_measure_setting)
    
    @Slot()
    def change_measure_setting(self, enable):
        self.mstart.clear()
        self.mend.clear()
        self.voice.clear()
        self.mstart.setReadOnly(not enable)
        self.mend.setReadOnly(not enable)
        self.voice.setReadOnly(not enable)

    def get_range(self):
        return [int(self.mstart.text()), int(self.mend.text())]

    def get_voice(self):
        return self.voice.text()

class KeyOptions(QWidget):

    keySignal = Signal(bool)

    def __init__(self,parent=None):
        super(KeyOptions, self).__init__(parent)
        self.layout = QBoxLayout(QBoxLayout.Direction.LeftToRight)

        self.keysiglabel = QLabel("Key Signature")
        self.keysig = QLineEdit(readOnly=True)
        self.notedurationlabel = QLabel("Default Note Duration")
        self.noteduration = QLineEdit(readOnly=True)

        self.layout.addWidget(self.keysiglabel)
        self.layout.addWidget(self.keysig)
        self.layout.addWidget(self.notedurationlabel)
        self.layout.addWidget(self.noteduration)

        self.setLayout(self.layout)

        self.keySignal.connect(self.change_key_setting)
    
    @Slot()
    def change_key_setting(self, enabled):
        self.keysig.clear()
        self.noteduration.clear()
        self.keysig.setReadOnly(not enabled)
        self.noteduration.setReadOnly(not enabled)

    def get_keysig(self):
        return self.keysig.text()

    def get_duration(self):
        return self.noteduration.text()

class ChordOptions(QWidget):

    chordSignal = Signal(bool)

    def __init__(self,parent=None):
        super(ChordOptions, self).__init__(parent)
        self.layout = QBoxLayout(QBoxLayout.Direction.LeftToRight)

        self.chordlvllabel = QLabel("Chord Level")
        self.chordlvl = QLineEdit(readOnly=True)
        self.chordsonly = QCheckBox("Chords Only")
        self.chordsonly.setEnabled(False)
        self.exceedingchords = QCheckBox("Ignore Exceeding Chords")
        self.exceedingchords.setEnabled(False)

        self.layout.addWidget(self.chordlvllabel)
        self.layout.addWidget(self.chordlvl)
        self.layout.addWidget(self.chordsonly)
        self.layout.addWidget(self.exceedingchords)

        self.setLayout(self.layout)

        self.chordSignal.connect(self.change_chord_setting)

    @Slot()
    def change_chord_setting(self, enable):
        self.chordlvl.clear()
        self.chordlvl.setReadOnly(not enable)
        self.chordsonly.setEnabled(enable)
        self.exceedingchords.setEnabled(enable)
        if not enable:
            self.chordsonly.setChecked(enable)
            self.exceedingchords.setChecked(enable)

    def get_chordlvl(self):
        return self.chordlvl.text()

    def get_chordsonly(self):
        return self.chordsonly.isChecked()

    def get_exceedingchords(self):
        return self.exceedingchords.isChecked()

class ButtonOptions(QWidget):

    buttonSignal = Signal(bool)

    def __init__(self,parent=None):
        super(ButtonOptions, self).__init__(parent)
        self.layout = QBoxLayout(QBoxLayout.Direction.LeftToRight)

        self.submit = QPushButton("Submit", self)
        self.submit.setEnabled(False)
        self.cancel = QPushButton("Cancel", self)
        self.copy = QPushButton("Copy", self)
        self.copy.setEnabled(False)

        self.layout.addWidget(self.submit)
        self.layout.addWidget(self.cancel)
        self.layout.addWidget(self.copy)

        self.setLayout(self.layout)

        self.buttonSignal.connect(self.change_button_enable)

    @Slot()
    def change_button_enable(self, enable):
        self.submit.setEnabled(enable)
        self.copy.setEnabled(enable)

class FileInput(QWidget):

    # create signals
    filenameSignal = Signal(str)
    inputpathSignal = Signal(str)

    def __init__(self, parent=None):
        super(FileInput, self).__init__(parent)

        self.layout = QBoxLayout(QBoxLayout.Direction.LeftToRight)

        # create key portions of widget
        self.inputlabel = QLabel("Input")
        self.inputPath = QLineEdit(readOnly=True)
        self.browsebutton = QPushButton("Browse", self)
        self.filename = None

        # wire up signals to slots
        self.browsebutton.clicked.connect(self.new_file)
        self.inputpathSignal.connect(self.inputPath.setText)

        # set layout
        self.layout.addWidget(self.inputlabel)
        self.layout.addWidget(self.inputPath)
        self.layout.addWidget(self.browsebutton)
        self.setLayout(self.layout)

    @Slot() 
    def new_file(self):
        filenametemp = QFileDialog.getOpenFileName(self, "Open Music File", ".", "*.mid *.midi *.mxl")
        filenametemp = filenametemp[0]
        if filenametemp != "":
            self.filename = filenametemp
            self.inputpathSignal.emit(self.filename)
            self.inputPath.repaint()
            self.filenameSignal.emit(self.filename)


class Form(QDialog):

    outputSignal = Signal(str)

    def __init__(self, parent=None):
        super(Form, self).__init__(parent)
        self.setWindowTitle("PMD Arpeggio Destroyer")

        self.layout = QBoxLayout(QBoxLayout.Direction.TopToBottom)

        self.fileinput = FileInput()
        self.instoptions = InstrumentOptions()
        self.measureoptions = MeasureOptions()
        self.keyoptions = KeyOptions()
        self.chordoptions = ChordOptions()
        self.buttons = ButtonOptions()
        self.outputbox = QPlainTextEdit(readOnly=True)

        self.layout.addWidget(self.fileinput)
        self.layout.addWidget(self.instoptions)
        self.layout.addWidget(self.measureoptions)
        self.layout.addWidget(self.keyoptions)
        self.layout.addWidget(self.chordoptions)
        self.layout.addWidget(self.buttons)
        self.layout.addWidget(self.outputbox)
        self.setLayout(self.layout)

        self.fileinput.filenameSignal.connect(self.read_in_filename)
        self.outputSignal.connect(self.outputbox.setPlainText)
        self.buttons.cancel.clicked.connect(self.close_program)
        self.buttons.submit.clicked.connect(self.execute_request)
        self.buttons.copy.clicked.connect(self.copy_all_text)

        self.pmdbox = PMDBox()

    @Slot()
    def read_in_filename(self, filename):
        
        self.change_form_enabled(False)

        self.outputSignal.emit("Loading in Music21 file")
        self.outputbox.repaint()

        # TODO: file safety check

        # TODO: musescore conversion

        if not self.pmdbox.gen_music21_obj(filename):

            self.outputSignal.emit("Error: something went wrong with processing the input file")
        else:
            self.outputSignal.emit("Ready")
            self.change_form_enabled(True)            

    @Slot()
    def close_program(self):
        sys.exit()

    @Slot()
    def execute_request(self):
        try:
            params = Params(
                inst_index=self.instoptions.get_selected_index(),
                range=self.measureoptions.get_range(),
                key_signature=self.keyoptions.get_keysig(),
                chords_only = self.chordoptions.get_chordsonly(),
                exceed_chords = self.chordoptions.get_exceedingchords()
            )

            chordlvl = self.chordoptions.get_chordlvl()
            if chordlvl != "":
                params.chord_level = int(chordlvl)

            voice = self.measureoptions.get_voice()
            if voice != "":
                params.voice = int(voice)

            duration = self.keyoptions.get_duration()
            if duration != "":
                params.note_duration = int(duration)

            output = self.pmdbox.process_request(params)
            self.outputSignal.emit(output)

        except (ValueError, TypeError) as e:
            output = f"Error: invalid parameter. The below exception may help:\n{str(e)}"
            self.outputSignal.emit(output)

    @Slot()
    def copy_all_text(self):
        self.outputbox.selectAll()
        self.outputbox.copy()


    def change_form_enabled(self,enabled):
        if enabled:
            instlist = self.pmdbox.get_insts()
        else:
            instlist = []
        self.instoptions.instrumentListSignal.emit(instlist)
        self.measureoptions.measureSignal.emit(enabled)
        self.keyoptions.keySignal.emit(enabled)
        self.chordoptions.chordSignal.emit(enabled)
        self.buttons.buttonSignal.emit(enabled)

if __name__ == "__main__":
    app = QApplication(sys.argv)
    app.setWindowIcon(QIcon("icon.png"))
    form = Form()
    form.resize(500,400)
    form.show()
    sys.exit(app.exec())
