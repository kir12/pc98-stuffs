
; Measures 1 to 9
T r8 o5 px-1 D-25 @13c c @13c px3 D0 @14c c c c c c c c c c c c c c c @14c c c c c c c c c c c c c c px-2 D-50 @13c c @13c px3 D0 @14c c c c c c c c c c c c c c c @14c c c c c c c c c c c c c c c c @14c c c c c c c c c c c c c c c c px0 D-84 @13c px3 D0 @14c c c c c c c c c c c c c c c px-2 D-50 @13c px3 D0 @14c c c c c c c c c c c c c c c px2 @32c c c px3 @14c px2 @32c c c px3 @14c px2 @32c c c px3 @14c px2 @32c c c c 
S r4 o5 px3 D0 @14c4 px0 @31c4 c4 c4 @31c4 c4 c4 c8 px3 @14c c @14c4 px0 @31c4 c4 c4 @31c4 c4 c4 c8 D-84 @13c c @13c4 D0 @31c4 c4 c4 px3 @14c4 px0 @31c4 c4 c8 D-84 @13c c px3 D0 @14c4 px0 @31c4 c4 c4 px3 @14c c c8 c c c8 c c c8 c c c c 
Q r4 o5 px0 D0 @31c1 r1 @31c1 r1 @31c1 @31c1 @31c1 @31c4 c4 c4 c4 
P r4 o5 px1 D0 @104c1 r1 @104c1 r1 @104c1 @104c1 @104c1 @104c1 
O r4 r1 r1 r1 r1 r1 r1 r1 r1 

; Measures 10 to 17
T o5 px3 D0 @14c c c c px2 @32c px3 @14c c c c c c c px2 @32c px3 @14c c c @14c c c c px2 @32c px3 @14c c c c c c c px2 @32c px3 @14c c c @14c c c c px2 @32c px3 @14c c c c c c c px2 @32c px3 @14c c c @14c c c c px2 @32c px3 @14c c c c c c c px2 @32c px3 @14c c c @14c c c c px2 @32c px3 @14c c c c c c c px2 @32c px3 @14c c c @14c c c c px2 @32c px3 @14c c c c c c c px2 @32c px3 @14c c c @14c c c c px2 @32c px3 @14c c c c c c c px2 @32c px3 @14c c c @14c c c c px2 @32c px3 @14c c c px-2 D-50 @13c c px1 D-10 @13c c px2 D0 @32c8 px0 D-84 @13c px-2 D-50 @13c 
S o5 px0 D0 @31c8 c c px3 @14c8 px0 @31c c c8 c c px3 @14c8 px0 @31c8 @31c8 c c px3 @14c8 px0 @31c c c8 c c px3 @14c8 px0 @31c8 @31c8 c c px3 @14c8 px0 @31c c c8 c c px3 @14c8 px0 @31c8 @31c8 c c px3 @14c8 px0 @31c c c8 c c px3 @14c8 px0 @31c8 @31c8 c c px3 @14c8 px0 @31c c c8 c c px3 @14c8 px0 @31c8 @31c8 c c px3 @14c8 px0 @31c c c8 c c px3 @14c8 px0 @31c8 @31c8 c c px3 @14c8 px0 @31c c c8 c c px3 @14c8 px0 @31c8 @31c8 c c px3 @14c8 px0 @31c c c8 c c D-30 @14c8 D0 @31c8 
Q o5 px1 D0 @104c4 px0 D-30 @14c2 c4 r4 @14c2 c4 r4 @14c2 c4 r4 @14c2 c4 r4 @14c2 c4 r4 @14c2 c4 r4 @14c2 c4 r4 @14c2&4 
P r1 r1 r1 r1 r1 r1 r1 r1 
O r1 r1 r1 r1 r1 r1 r1 r1 

; Measures 18 to 25
T o5 px3 D0 @14c c c c px2 @32c px3 @14c c c c c c c px2 @32c px3 @14c c c @14c c c c px2 @32c px3 @14c c c c c c c px2 @32c px3 @14c c c @14c c c c px2 @32c px3 @14c c c c c c c px2 @32c px3 @14c c c @14c c c c px2 @32c px3 @14c c c c c c c px2 @32c px3 @14c c c @14c c c c px2 @32c px3 @14c c c c c c c px2 @32c px3 @14c c c @14c c c c px2 @32c px3 @14c c c c c c c px2 @32c px3 @14c c c @14c c c c px2 @32c px3 @14c c c c c c c px2 @32c px3 @14c c c @14c c c c px2 @32c px3 @14c c c px-2 D-50 @13c c px1 D-10 @13c c px2 D0 @32c8 px0 D-84 @13c px-2 D-50 @13c 
S o5 px0 D0 @31c8 c c px3 @14c8 px0 @31c c c8 c c px3 @14c8 px0 @31c8 @31c8 c c px3 @14c8 px0 @31c c c8 c c px3 @14c8 px0 @31c8 @31c8 c c px3 @14c8 px0 @31c c c8 c c px3 @14c8 px0 @31c8 @31c8 c c px3 @14c8 px0 @31c c c8 c c px3 @14c8 px0 @31c8 @31c8 c c px3 @14c8 px0 @31c c c8 c c px3 @14c8 px0 @31c8 @31c8 c c px3 @14c8 px0 @31c c c8 c c px3 @14c8 px0 @31c8 @31c8 c c px3 @14c8 px0 @31c c c8 c c px3 @14c8 px0 @31c8 @31c8 c c px3 @14c8 px0 @31c c c8 c c D-30 @14c8 D0 @31c8 
Q o5 px1 D0 @104c4 px0 D-30 @14c2 c4 r4 @14c2 c4 r4 @14c2 c4 r4 @14c2 c4 r4 @14c2 c4 r4 @14c2 c4 r4 @14c2 c4 r4 @14c2&4 
P r1 r1 r1 r1 r1 r1 r1 r1 
O r1 r1 r1 r1 r1 r1 r1 r1 

; Measures 26 to 33
T o5 px0 D0 @31c8 c c px2 @32c8 px0 @31c c c8 c c px2 @32c8 px0 @31c8 @31c8 c c px2 @32c8 px0 @31c c c8 c c px2 @32c8 px0 @31c8 @31c8 c c px2 @32c8 px0 @31c c c8 c c px2 @32c8 px0 @31c8 @31c8 c c px2 @32c8 px0 @31c c c8 c c px2 @32c8 px0 @31c8 @31c8 c c px2 @32c8 px0 @31c c c8 c c px2 @32c8 px0 @31c8 @31c8 c c px2 @32c8 px0 @31c c c8 c c px2 @32c8 px0 @31c8 @31c8 c c px2 @32c8 px0 @31c c c8 c c px2 @32c8 px0 @31c8 px2 @32c c c px0 @31c px2 @32c c c px0 @31c px2 @32c c c px0 @31c px2 @32c c c c 
S o5 px0 D0 @24c4 c4 c4 c4 @24c4 c4 c4 c4 @24c4 c4 c4 c4 @24c4 c4 c4 c4 @24c4 c4 c4 c4 @24c4 c4 c4 c4 @24c4 c4 c4 c4 @31c8 c8 @24c8 @31c8 c8 c8 @24c8 @31c8 
Q o5 px1 D0 @104c1 r1 r1 r1 r1 r1 r1 px0 @24c8 c4 c8 c8 c4&8 
P r1 r1 r1 r1 r1 r1 r1 r1 
O r1 r1 r1 r1 r1 r1 r1 r1 

; Measures 34 to 41
T o5 px0 D0 @31c8 c c px2 @32c8 px0 @31c c c8 c c px2 @32c8 px0 @31c8 @31c8 c c px2 @32c8 px0 @31c c c8 c c px2 @32c8 px0 @31c8 @31c8 c c px2 @32c8 px0 @31c c c8 c c px2 @32c8 px0 @31c8 @31c8 c c px2 @32c8 px0 @31c c c8 c c px2 @32c8 px0 @31c8 @31c8 c c px2 @32c8 px0 @31c c c8 c c px2 @32c8 px0 @31c8 @31c8 c c px2 @32c8 px0 @31c c c8 c c px2 @32c8 px0 @31c8 @31c8 c c px2 @32c8 px0 @31c c c8 c c px2 @32c8 px0 @31c8 D-84 @13c8 px2 D0 @32c px0 D-84 @13c8 px2 D0 @32c px0 @31c8 c4 px2 @32c c c c 
S o5 px0 D0 @24c4 c4 c4 c4 @24c4 c4 c4 c4 @24c4 c4 c4 c4 @24c4 c4 c4 c4 @24c4 c4 c4 c4 @24c4 c4 c4 c4 @24c4 c4 c4 c4 @31c8&16 c8&16 px1 @104c8 c2 
Q o5 px1 D0 @104c1 r1 r1 r1 r1 r1 r1 @104c8&16 c2&4&16 
P r1 r1 r1 r1 r1 r1 r1 r1 
O r1 r1 r1 r1 r1 r1 r1 r1 

; Measures 42 to 49
T o5 px3 D0 @14c c c c px2 @32c px3 @14c c c c c c c px2 @32c px3 @14c c c @14c c c c px2 @32c px3 @14c c c c c c c px2 @32c px3 @14c c c @14c c c c px2 @32c px3 @14c c c c c c c px2 @32c px3 @14c c c px2 @32c8 px0 @31c8 px2 @32c8 px0 D-84 @13c8 px2 D0 @32c8 px0 D-84 @13c c px2 D0 @32c px3 @14c px2 @32c c px3 @14c c c c px2 @32c px3 @14c c c c c c c px2 @32c px3 @14c c c @14c c c c px2 @32c px3 @14c c c c c c c px2 @32c px3 @14c c c @14c c c c px2 @32c px3 @14c c c c c c c px2 @32c px3 @14c c px2 @32c @32c c c px3 @14c px2 @32c c c px3 @14c px2 @32c c c px3 @14c px2 @32c c c c 
S o5 px0 D0 @31c8 c c px3 @14c8 px0 @31c c c8 c c px3 @14c8 px0 @31c8 @31c8 c c px3 @14c8 px0 @31c c c8 c c px3 @14c8 px0 @31c8 @31c8 c c px3 @14c8 px0 @31c c c8 c c px3 @14c8 px0 @31c8 @31c4 c8 c8 c8 c8 px3 @14c8 c c px0 @31c8 c c px3 @14c8 px0 @31c c c8 c c px3 @14c8 px0 @31c8 @31c8 c c px3 @14c8 px0 @31c c c8 c c px3 @14c8 px0 @31c8 @31c8 c c px3 @14c8 px0 @31c c c8 c c px3 @14c8 px0 @31c px3 @14c @14c c c8 c c c8 c c c8 c c c c 
Q o5 px1 D0 @104c4 px0 D-30 @14c2 c4 r4 @14c2 c4 r4 @14c2 c4 px1 D0 @104c4 c4 c4 px0 D-30 @14c4 px1 D0 @104c4 px0 D-30 @14c2 c4 r4 @14c2 c4 r4 @14c2 c4 D0 @31c4 D-30 @14c4 D0 @31c4 D-30 @14c4 
P r1 r1 r1 r1 r1 r1 r1 o5 px1 D0 @104c4 px0 @31c4 px1 @104c4 px0 @31c4 
O r1 r1 r1 r1 r1 r1 r1 r2&4 o5 px1 D0 @104c4 

; Measures 50 to 57
T o5 px3 D0 @14c c c c px2 @32c px3 @14c c c c c c c px2 @32c px3 @14c c c @14c c c c px2 @32c px3 @14c c c c c c c px2 @32c px3 @14c c c @14c c c c px2 @32c px3 @14c c c c c c c px2 @32c px3 @14c c c px2 @32c8 px0 @31c8 px2 @32c8 px0 D-84 @13c8 px2 D0 @32c8 px0 D-84 @13c c px2 D0 @32c px3 @14c px2 @32c c px3 @14c c c c px2 @32c px3 @14c c c c c c c px2 @32c px3 @14c c c @14c c c c px2 @32c px3 @14c c c c c c c px2 @32c px3 @14c c c @14c c c c px2 @32c px3 @14c c c c c c c px2 @32c px3 @14c c px2 @32c px-2 D-50 @13c8 px2 D0 @32c px-2 D-50 @13c8 c px2 D0 @32c8 px-1 D-25 @13c c c c px-2 D-50 @13c c px0 D-84 @13c c 
S o5 px0 D0 @31c8 c c px3 @14c8 px0 @31c c c8 c c px3 @14c8 px0 @31c8 @31c8 c c px3 @14c8 px0 @31c c c8 c c px3 @14c8 px0 @31c8 @31c8 c c px3 @14c8 px0 @31c c c8 c c px3 @14c8 px0 @31c8 @31c4 c8 c8 c8 c8 px3 @14c8 c c px0 @31c8 c c px3 @14c8 px0 @31c c c8 c c px3 @14c8 px0 @31c8 @31c8 c c px3 @14c8 px0 @31c c c8 c c px3 @14c8 px0 @31c8 @31c8 c c px3 @14c8 px0 @31c c c8 c c px3 @14c8 px0 @31c px3 @14c px0 @31c8&16 c8 c8&16 c c c c c c c c 
Q o5 px1 D0 @104c4 px0 D-30 @14c2 c4 r4 @14c2 c4 r4 @14c2 c4 px1 D0 @104c4 c4 c4 px0 D-30 @14c4 px1 D0 @104c4 px0 D-30 @14c2 c4 r4 @14c2 c4 r4 @14c2 c4 px1 D0 @104c2 c2 
P r1 r1 r1 r1 r1 r1 r1 r1 
O r1 r1 r1 r1 r1 r1 r1 r1 

; Measures 58 to 65
T o5 px3 D0 @14c c c c px2 @32c px3 @14c c c c c c c px2 @32c px3 @14c c c @14c c c c px2 @32c px3 @14c c c c c c c px2 @32c px3 @14c c c @14c c c c px2 @32c px3 @14c c c c c c c px2 @32c px3 @14c c c @14c c c c px2 @32c px3 @14c c c c c c c px2 @32c px3 @14c c c @14c c c c px2 @32c px3 @14c c c c c c c px2 @32c px3 @14c c c @14c c c c px2 @32c px3 @14c c c c c c c px2 @32c px3 @14c c c @14c c c c px2 @32c px3 @14c c c c c c c px2 @32c px3 @14c c c px2 @32c c c px3 @14c px2 @32c c c px3 @14c px2 @32c c c px3 @14c px2 @32c c c c 
S o5 px0 D0 @31c8 c c px3 @14c8 px0 @31c c c8 c c px3 @14c8 px0 @31c8 @31c8 c c px3 @14c8 px0 @31c c c8 c c px3 @14c8 px0 @31c8 @31c8 c c px3 @14c8 px0 @31c c c8 c c px3 @14c8 px0 @31c8 @31c8 c c px3 @14c8 px0 @31c c c8 c c px3 @14c8 px0 @31c8 @31c8 c c px3 @14c8 px0 @31c c c8 c c px3 @14c8 px0 @31c8 @31c8 c c px3 @14c8 px0 @31c c c8 c c px3 @14c8 px0 @31c8 @31c8 c c px3 @14c8 px0 @31c c c8 c c px3 @14c8 px0 @31c8 px3 @14c c c px0 @31c px3 @14c c c px0 @31c px3 @14c c c px0 @31c px3 @14c c c c 
Q o5 px1 D0 @104c4 px0 D-30 @14c2 c4 r4 @14c2 c4 r4 @14c2 c4 r4 @14c2 c4 r4 @14c2 c4 r4 @14c2 c4 r4 @14c2 c4 D0 @31c8 c8 D-30 @14c8 D0 @31c8 c8 c8 D-30 @14c8 D0 @31c8 
P r1 r1 r1 r1 r1 r1 r1 r1 
O r1 r1 r1 r1 r1 r1 r1 r1 
