'''
This is xml_anal.py, a script which generates PMD-compatible percussion code
given a MIDI, Musescore3, or MusicXML source file.
Normally I would've used music21, however they notably do not support
parsing percussion as of 1/17/21, so an alternate solution of
directly parsing the XML itself was devised.

Current Limitations: Only one drum MIDI channel is supported, and
each measure requires exactly two voices. (I will get that resolved ASAP)
'''


import xml.etree.ElementTree as ET
import zipfile
import os
import sys
import subprocess
import math


class VoiceStruct:

    def __init__(self, voice):
        self.voice = voice
        # keep track of next element index to inspect
        self.current_idx = 0
        # (duration to limit for, duration already covered, delay active?)
        self.disable = [0, 0, False]

    # retrieves information necessary for computing a single note
    def collect_note_data(self, quarter_duration):
        # special case: no second voice
        # return whole length instead
        if len(self.voice) == 0:
            return {
                "inst_num": 0,
                "note_duration": 999,  # TODO: may break something later
                "num_chord_notes": 0
            }
        # capture note duration and search for instrument
        note_duration = int(self.voice[self.current_idx].find("duration").text)
        inst_search = self.voice[self.current_idx].find("instrument")
        num_chord_notes = 1
        if inst_search is None:  # if no inst, then just instrument
            inst_num = [0]
        else:  # capture base note and next chords
            inst_num = [drum_trans[inst_search.attrib["id"].split("-")[1]]]
            # conduct search for chords
            while self.current_idx + num_chord_notes < len(self.voice) and \
                    self.voice[self.current_idx + num_chord_notes].find("chord") is not None:
                next_note = self.voice[self.current_idx + num_chord_notes]
                tmp_inst_num = drum_trans[next_note.find("instrument").attrib["id"].split("-")[1]]
                if tmp_inst_num not in inst_num:
                    inst_num.append(tmp_inst_num)
                num_chord_notes += 1
        # return everything as dictionary
        self.note_prop = {
            "inst_num": inst_num,
            "note_duration": note_duration,
            "num_chord_notes": num_chord_notes-1
        }

    # increments voice_idx if no delay enforced
    # otherwise increments voice_data by pc_98 duration instead
    # also resets voice data when necessary
    def advance_note(self, f_duration):
        if not self.disable[2]:
            self.current_idx += 1 + self.note_prop["num_chord_notes"]
        else:
            self.disable[0] = self.note_prop["note_duration"]
            self.disable[1] += f_duration
            # special case: voice timing properly synced and ready to proceed
            if self.disable[0] == self.disable[1]:
                self.disable = [0, 0, False]
                self.current_idx += 1 + self.note_prop["num_chord_notes"]


drum_trans = {
    "I29": 1,
    "I36": 1,  # Acoustic Bass Drum",
    "I37": 1,  # "Bass Drum 1",
    "I38": 32,  # "Side Stick",
    "I39": 2,  # "Acoustic Snare",
    "I40": 66,  # "Instrument 40", (both snare drums)
    "I41": 64,  # "Electric Snare",
    "I42": 4,  # "Low Floor Tom",
    "I43": 128,  # "Closed Hi-Hat",
    "I44": 16,  # "High Floor Tom"
    "I45": 128,  # "Pedal Hi-Hat"
    "I46": 4,  # "Low Tom",
    "I47": 256,  # "Open Hi-Hat",
    "I48": 8,  # "Low-Mid Tom",
    "I49": 8,  # "Hi-Mid Tom",
    "I50": 512,  # "Crash Cymbal 1",
    "I51": 16,  # "High Tom",
    "I52": 1024,  # "Ride Cymbal 1",
    "I53": "Chinese Cymbal",
    "I54": "Ride Bell",
    "I55": "Tambourine",
    "I56": 1536,  # "Splash Cymbal",
    "I57": "Cowbell",
    "I58": 512,  # Crash Cymbal 2",
    "I60": 1024,  # "Ride Cymbal 2",
    "I64": "Open Hi Conga",
    "I65": "Low Conga"
}


# extracts drum channel from MusicXML file
def get_drum_channel(mxl_dir):
    # extract xml file from mxl file (treat as zip)
    archive = zipfile.ZipFile(mxl_dir, 'r')
    xml_dir = os.path.splitext(os.path.basename(mxl_dir))[0]+".xml"
    xml_file = archive.open(xml_dir)
    # open file in ElementTree, extract drumset ID number
    tree = ET.parse(xml_file)
    root = tree.getroot()

    # TODO: add support for multiple drum lines
    part_lists = root.findall("./part-list/score-part")
    drum_index = []
    # key assumption: only percussion will have multiple instruments in a part
    for i in range(len(part_lists)):
        if len(part_lists[i].findall("./score-instrument")) > 1:
            drum_index.append(i)
    if len(drum_index) == 0:
        raise AttributeError("No valid drumset channel was found.")
    else:
        print(
            "Drum channels found at # " +
            ", ".join(str(i) for i in drum_index)
        )
        return [root.findall("./part")[i] for i in drum_index]


# TODO: how to handle dotted notes?
# given a measure, creates a new pattern for it
def create_new_pattern(ms_list, quarter_duration):

    output = ""
    voices = []
    # parse out measures into voices
    for ms in ms_list:
        for i in range(1, 5):
            temp_voice = VoiceStruct(
                ms.findall("note[voice='" + str(i) + "']"))
            if len(temp_voice.voice) != 0:
                voices.append(temp_voice)

    # while voice_1.current_idx < len(voice_1.voice) or \
    #        voice_2.current_idx < len(voice_2.voice):
    while any(vc.current_idx < len(vc.voice) for vc in voices):
        # generate new note object
        for vc in voices:
            vc.collect_note_data(quarter_duration)

        # iterate through channels WITHOUT active disable
        # add their instruments, capture shortest duration
        base_duration = float('inf')
        base_indices = []
        inst_set = set()
        for i in range(len(voices)):
            vc = voices[i]
            if not vc.disable[2]:
                # add all unique instruments to set
                inst_set = inst_set.union(set(vc.note_prop["inst_num"]))
                # check for smaller note duration
                if vc.note_prop["note_duration"] <= base_duration:
                    base_duration = vc.note_prop["note_duration"]
                    if vc.note_prop["note_duration"] == base_duration:
                        base_indices.append(i)
                    else:
                        base_indices = [i]

        # generate final PC-98 instrument
        pc98_inst = sum(inst_set)

        # check if base_duration exceeds limits of any active disables
        # if this is true, then get smallest difference and
        # replace with base_duration
        cut_duration = min([
            v.disable[0] - v.disable[1] for v in voices
            if v.disable[2] and
            v.disable[1] + base_duration > v.disable[0]],
            default=base_duration)

        # flag losing channels as getting a disable
        for i in range(len(voices)):
            if i not in base_indices and not voices[i].disable[2]:
                voices[i].disable[2] = True

        # limitation exceed detected!
        # mitigation: set base duration to remaining difference,
        # flag disable as true for winning channels
        if cut_duration != base_duration:
            base_duration = cut_duration
            for i in base_indices:
                voices[i].disable[2] = True

        # convert to PC98 compatible duration 
        f_duration = base_duration
        # dotted note detected! (edge case #2, TODO)
        dotted = False
        if (base_duration < quarter_duration and
            quarter_duration % base_duration != 0) or \
                (base_duration > quarter_duration and
                    base_duration % quarter_duration != 0):
            dotted = True
        base_duration = int(4 * (quarter_duration/base_duration))

        # if not math.log(base_duration, 2).is_integer():
        #     new_duration = math.pow(2, math.ceil(math.log(base_duration, 2))) 
        # compute final inst type and print note to console
        if pc98_inst == 0:
            output += "r"
        else:
            output += "@" + str(pc98_inst) + "c"
        output += str(base_duration) + (". " if dotted else " ")

        # advance index only if delay not enforced
        # TODO: premature resetting
        for vc in voices:
            vc.advance_note(f_duration)

        '''
        algorithm pseudocode, updated for atbitrary voices
        start at the beginning of all voices
        while any notes remain on any voices:
            compute each channel's drum anount/duration (with chords)
            (note: this is data about ONE note in each channel)
        for the channels that DO NOT have active disable:
            add all the inst numbers
            pick the channel(s) with the shortest duration, store as A
        flag all the losing channels as getting a disable

        for all channels that DO HAVE active disable:
            check if adding A exceeds disable limit
            if it does, store offending channel with difference
        if there were any offending channels:
            set output duration to SMALLEST difference
            flag winning channel as getting delay
        else:
            set output duration to A
            

        write the resulting note to console with output duration
        advance the winning channel(s) to the next note
        activate delays for remaining channels with longer duration
        advance delays for channels with active disables

        '''

        '''
        algorithm pseudocode
        start at beginning of both voices
        while notes remain on both voices:
            for each voice, if there's a note on beat:
                compute each drum amount separately, accounting for chords
            compare note types (eigth, fourth, etc) and pick the smaller one
            write to output string
            go to the next note for the smaller duration, keep the bigger
            duration stored for when they next match up
        '''
    return output


# processes drum measures and determines which ones are duplicates
# orders generation of new measures whenever necessary
def drum_analysis(drum_channels):
    part_num = 0  # next drum part to be used
    ms_num = 1
    # extract all measures for each part, store in 2D list
    ms_list = [part.findall("measure") for part in drum_channels]
    quarter_duration = None  # how many duration goes into a quarter note
    # call up drum pattern, format in PC-98 compatible format
    pattern_library = []
    for ms_idx in range(len(ms_list[0])):
        ms_focus = [channel[ms_idx] for channel in ms_list]
        # capture "time signature" aka duration points/quarter note
        for ms in ms_focus:
            if ms.find("attributes") is not None:
                quarter_duration = int(ms.find("attributes/divisions").text)
                break

        # create drum pattern and search for it
        drum_pattern = create_new_pattern(ms_focus, quarter_duration)
        if drum_pattern not in pattern_library:
            print("R"+str(part_num)+" "+drum_pattern)
            pattern_library.append(drum_pattern)
            part_num += 1
        else:
            repeat_idx = str(pattern_library.index(drum_pattern))
            print("; Measure " + str(ms_num) + " repeat of R"+repeat_idx)
        ms_num += 1


if __name__ == "__main__":
    if len(sys.argv) == 1:
        print('''xml_anal.py: Automatic generation of PMD-compatible drum code.
        The only dependency is Musescore. (for conversions to MusicXML)\n
        USAGE: python xml_anal.py [FILE]
        [FILE] can be a Musescore file, a MIDI file, or a MusicXML file.

        Calling xml_anal.py without [FILE] will bring up this help message.
        ''')
        exit(1)
    # split filename/extension, then do input validation
    f_split = os.path.splitext(sys.argv[1])
    if f_split[1] in [".mscz", ".mid", ".mxl"]:
        if f_split[1] in [".mscz", ".mid"]:  # run musescore exporter first
            subprocess.run(["mscore", "-o", f_split[0] + ".mxl", sys.argv[1]])
        # actual program runs
        drum_channels = get_drum_channel(f_split[0] + ".mxl")
        drum_analysis(drum_channels)
    else:
        print("Error: an invalid file was supplied.")
