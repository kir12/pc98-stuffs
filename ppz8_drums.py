"""Auto-generate PPZ8 drum part for songs."""
import xml.etree.ElementTree as ET
import zipfile
import os
import math
import subprocess
import argparse
import json
from copy import deepcopy
from itertools import groupby
from music21 import converter
from music21.note import Rest
from music21.percussion import PercussionChord
from music21.chord import Chord

# constants to denote drum types for the sake of readability
BASS_DRUM = 'BASS_DRUM'
SNARE_DRUM = 'SNARE_DRUM'
FLOOR_TOM = 'FLOOR_TOM'
LOW_MID_TOM = 'LOW_MID_TOM'
MID_HIGH_TOM = 'MID_HIGH_TOM'
# MID_TOM = 'MID_TOM'
HIGH_TOM = 'HIGH_TOM'
CLOSED_HI_HAT = 'CLOSED_HI_HAT'
OPEN_HI_HAT = 'OPEN_HIGH_HAT'
PEDAL_HI_HAT = 'PEDAL_HI_HAT'
CRASH_CYMBAL = 'CRASH_CYMBAL'
RIDE_CYMBAL = 'RIDE_CYMBAL'
SIDE_STICK = 'SIDE_STICK'
SPLASH_CYMBAL = 'SPLASH_CYMBAL'
COWBELL = 'COWBELL'
HANDCLAP = 'HANDCLAP'
LOW_TOM = 'LOW_TOM'
REST = 'REST'
CHINESE_CYMBAL = "CHINESE_CYMBAL"
RIDE_BELL = "RIDE_BELL"
TAMBOURINE = "TAMBOURINE"
SURDO_MUTE = "SURDO_MUTE"
SURDO_OPEN = "SURDO_OPEN"

# neater version of drum_trans
MIDI_CONVERSIONS = {
    35: BASS_DRUM, # acoustig
    36: BASS_DRUM,
    37: SIDE_STICK,
    38: SNARE_DRUM,  # acoustic
    40: SNARE_DRUM, # electric
    39: HANDCLAP,
    41: FLOOR_TOM, # high
    43: FLOOR_TOM, #low
    42: CLOSED_HI_HAT,
    44: PEDAL_HI_HAT,
    46: OPEN_HI_HAT,
    45: LOW_TOM,
    47: LOW_MID_TOM, 
    48: MID_HIGH_TOM, 
    50: HIGH_TOM,
    49: CRASH_CYMBAL, # crash cymbal 1
    57: CRASH_CYMBAL,  # crash cymbal 2
    51: RIDE_CYMBAL,
    59: RIDE_CYMBAL, # ride_cymbal 2
    52: CHINESE_CYMBAL,
    53: RIDE_BELL,
    54: TAMBOURINE,
    55: SPLASH_CYMBAL,
    56: COWBELL,
    # 58: vibraslap
    # 60: high bongo
    # 61: low bongo
    # 62: mute hi conga
    # 63: open hi conga
    # 64: low conga
    # 65: high timbale
    # 66: low timbale
    # 67 ~ 81 etc are not in scope of PMD
    # consult a MIDI percussion key map for more info
    86: SURDO_MUTE,
    87: SURDO_OPEN,

    27: BASS_DRUM,
    70: CLOSED_HI_HAT,
    69: PEDAL_HI_HAT

}

# instruments that need to be higher than any other instrument when sorted by offset
# this gives them the highest chance of getting their own channel
# (primarily applies to cymbals)
PRIORITY = {
    SNARE_DRUM: 1,
    BASS_DRUM: 1,
    SURDO_OPEN: 1,
    OPEN_HI_HAT: 2,
    RIDE_CYMBAL: 3,
    SPLASH_CYMBAL: 4,
    COWBELL: 5,
    CRASH_CYMBAL: 6
}

# given an array of part indices, extracts them via old-school ElementTree
# NOTE: measure extraction does NOT happen here
def extract_xml_parts(file_dir):

    # extract xml file from mxl file (treat as zip)
    archive = zipfile.ZipFile(file_dir, 'r')
    xml_dir = os.path.splitext(os.path.basename(file_dir))[0]+".xml"
    try:
        xml_file = archive.open(xml_dir)
    except:
        xml_file = archive.open("score.xml")

    # open file in ElementTree, extract specific parts
    tree = ET.parse(xml_file)
    root = tree.getroot()

    # perform our own search for drum instruments
    # reason: Music21 and ElementTree represent multiple-staffed
    # instruments differently.
    part_lists = root.findall("./part-list/score-part")
    drum_index = []
    for i in range(len(part_lists)):
        # add part if more than one inst or part has unpitched inst
        if len(part_lists[i].findall("./score-instrument")) > 1 or \
                part_lists[i].find("./midi-instrument").find("./midi-unpitched") is not None:
            drum_index.append(i)

    return [root.findall("./part")[i] for i in drum_index]


# extracts parts via music21, and calls xml method
def check_for_drums(file_dir):
    output = {"music21": [], "xml": []}
    drum_part_idx = []

    # initialize key music21 assets
    print("Opening MusicXML file in music21... ", end="", flush=True)
    score = converter.parse(file_dir)
    print("Done!")
    for i in range(len(score.parts)):
        part = score.parts[i]
        m_part = part.getElementsByClass("Measure")

        # if percussion clef, add part and index
        if any(obj.__class__.__name__ == "PercussionClef"
                for obj in m_part[0]):
            output["music21"].append(m_part)
            drum_part_idx.append(i)

    # grab xml part data
    output["xml"] = extract_xml_parts(file_dir)
    return output


# check if terminating (a.k.a. triplet)
# source: https://stackoverflow.com/questions/14481054/is-there-a-way-to-tell-if-a-decimal-is-terminating-or-repeating-in-c
def isTerminating(numerator, denominator):
    while denominator % 2 == 0:
        denominator /= 2
    while denominator % 5 == 0:
        denominator /= 5
    return denominator == 1


# given a duration where 1.0=quarter note,
# calculate/return PC-98 respective melody
def gen_pc98_duration(duration):
    def pc98(x): return (1.0/x)*4.0
    output = pc98(duration)
    # most cases: will fall evenly under a regular note
    if abs(round(output) - output) < 0.00001:
        return str(round(output))

    # special case: triplets
    # dump into dictionary
    frac = duration.as_integer_ratio()
    if not isTerminating(frac[0], frac[1]):
        raise AttributeError("fix your shit")
        # some return here

    # final case: dotted notes of some kind
    # idea: lop off notes that cleanly fall under quarter system
    # until we get to zero
    output_chk = duration
    duration_chain = []
    while output_chk > 0:
        subportion = 2 ** math.floor(math.log2(output_chk))
        duration_chain.append(subportion)
        output_chk -= subportion
    return "&".join(str(int(pc98(note))) for note in duration_chain)


# given a set of measures, generates a PMD compatible pattern
# TODO: separate music21 and xml into voices
def new_pattern(measures, flag_unknown_instruments, ppz8_config, data_tracker):
    # further strain out measures to individual voices
    # at this point, it is just notes and rests
    voices = {
        "music21": [],
        "xml": []
    }

    # helper method for reducing chords to standard Notes
    def process_m21(nr_data):
        i = 0
        while i < len(nr_data):
            cname = nr_data[i].__class__
            # expand chords if needed
            if cname in [PercussionChord, Chord]:
                expand_chord = list(nr_data[i])
                for note in expand_chord:
                    note.offset = nr_data[i].offset
                nr_data = nr_data[0:i] + expand_chord + nr_data[i+1:]
                i -= 1
            # tremelo check
            elif len(nr_data[i].expressions) > 0:
                # extract tremelos (should be only one)
                tremelo_lst = [cl for cl in nr_data[i].expressions if cl.__class__.__name__ == 'Tremolo']
                if len(tremelo_lst) > 0:
                    # determine tremelo type and duration of new notes
                    tr_num = tremelo_lst[0].numberOfMarks
                    note_num = 2**tr_num
                    new_duration = duration.Duration(quarterLength=nr_data[i].duration.quarterLength/note_num)
                    expand_tremelo = []
                    # construct notes and apply to nr data
                    for a in range(note_num):
                        new_note = deepcopy(nr_data[i])
                        new_note.duration = new_duration
                        new_note.expressions = []
                        new_note.offset = nr_data[i].offset + (a*new_duration.quarterLength)
                        expand_tremelo.append(new_note)
                    nr_data = nr_data[0:i] + expand_tremelo + nr_data[i+1:]
                    i -= 1
            i += 1

        return nr_data

    # strain out music21
    for measure in measures["music21"]:
        if len(list(measure.voices)) > 0:
            voices["music21"] += [process_m21(list(voice.notesAndRests))
                                  for voice in list(measure.voices)]
        else:
            voices["music21"] += [process_m21(list(measure.notesAndRests))]

    # strain out musicxml (maybe clean out later?)
    for idx, measure in enumerate(measures["xml"]):
        for i in range(1, 5):
            temp_voice = measure.findall("note[voice='" + str(i) + "']")
            # if notes exist, then convert them to PC-98 notes
            if len(temp_voice) != 0:
                inst_rep = []
                for note in temp_voice:
                    inst_search = note.find("instrument")
                    # inst --> PC-98 (using instrument attribute)

                    if inst_search is None:
                        inst_rep.append(REST)
                    else:
                        inst_id = inst_search.attrib["id"]
                        inst_id = inst_id.split("-")[1].replace("I","")

                        if not inst_id.isdigit():
                            raise AttributeError(f"Invalid Instrument {inst_id}, voice {i}, measure {measure.attrib['number']}")
                        inst_id = int(inst_id) - 1  # XML midi insts are 1 higher up than standard, TODO check consistency
                        # TODO: extend support for song config file for wonky other midi insts
                        if flag_unknown_instruments and inst_id not in MIDI_CONVERSIONS:
                            raise AttributeError(f"Bad Instrument {inst_id} at Voice {i} at measure {measure.attrib['number']}")
                        inst_num = MIDI_CONVERSIONS.get(inst_id, REST)

                        # add notes as necessary for tremolo
                        tremelo_check = note.find(".//tremolo")
                        if tremelo_check is not None:
                            num_notes = 2**int(tremelo_check.text)
                        else:
                            num_notes = 1
                        for a in range(num_notes):
                            inst_rep.append(inst_num)
                voices["xml"].append(inst_rep)

    # merge musicxml instrument data with music21 note data
    merged_results = []
    for note_voice, inst_voice in zip(voices["music21"], voices["xml"]):
        for note, inst in zip(note_voice, inst_voice):
            merged_results.append((note, inst))

    # apply sort based on music21 offset, removing any rests
    total_length = measures["music21"][0].duration.quarterLength
    merged_results.sort(key=lambda note: (note[0].offset, PRIORITY.get(note[1], 0)))
    merged_results = [elem for elem in merged_results if type(elem[0]) != Rest]
    # re-group them strictly by offset, and eliminate duplicate instruments
    offset_grouped_results = [list(j) for i, j in groupby(merged_results, key=lambda elem: elem[0].offset)]
    offset_grouped_results = [list({drum_evt[1]: drum_evt for drum_evt in offset}.values()) for offset in offset_grouped_results]
    # now regroup them such that every first element in each offset group lands in PPZ8 channel #1 and so on
    # result: ppz8 channel 1 is most populated, followed by 2 and so on...
    channel_grouped_results = [[],[],[],[],[],[],[],[]]  # corresponding to 8 PPZ8 channels
    for offset_grp in offset_grouped_results:
        for channel_idx, drum_evt in enumerate(offset_grp):
            channel_grouped_results[channel_idx].append(drum_evt)
    num_filled = len([elem for elem in channel_grouped_results if elem != []])

    channel_data = []

    for channel_num, channel in enumerate(channel_grouped_results):
        output = ""
        # if no notes at all
        if len(channel) == 0:
            output += f"r{gen_pc98_duration(total_length)} "
        past_inst = REST
        # iterate through each note in channel
        for note_evt_idx, note_evt in enumerate(channel):
            # if first note event doesn't actually start here, add a rest of duration from start to offset of first note
            if note_evt_idx == 0 and note_evt[0].offset != 0:
                duration = gen_pc98_duration(note_evt[0].offset)
                output += f"r{duration} "

            # compute length of note by subtract offset of either next note event or end of the channel
            next_offset = channel[note_evt_idx+1][0].offset if note_evt_idx < len(channel)-1 else total_length
            offset_diff = next_offset - note_evt[0].offset
            duration = gen_pc98_duration(offset_diff)

            # handle behavior on drum instrument that doesn't have supplied instrument
            # (NOT THE SAME AS UNDEFINED INSTRUMENT!)
            if not ppz8_config["warn_no_sample"]:
                instrument = ppz8_config["ppz8_definitions"][note_evt[1]]
            else:
                instrument = ppz8_config["ppz8_definitions"].get(note_evt[1], REST)
                if instrument == REST:
                    print(f"WARNING: A patch was not supplied for {note_evt[1]}")

            octave = f"o{ppz8_config['octave']}"
            panning = f"px{ppz8_config['soundspace'].get(note_evt[1], 0)}"
            detune = ppz8_config["detune_settings"].get(note_evt[1], 0)

            # append to output
            # only print panning/detune if new instrument
            if past_inst != note_evt[1]:
                if octave != data_tracker[channel_num]["octave"]:
                    output += f"{octave} "
                    data_tracker[channel_num]["octave"] = octave
                # TODO: volume adjustment

                if panning != data_tracker[channel_num]["panning"]:
                    output += f"{panning} "
                    data_tracker[channel_num]["panning"] = panning 

                # optionally input detune settings if detune options are being changed
                if detune != data_tracker[channel_num]["detune"]:
                    output += f"D{detune} "
                    data_tracker[channel_num]["detune"] = detune

                output += f"@{instrument}" if instrument != REST else "r"
                past_inst = note_evt[1]
            output += f"{'c' if instrument != REST else ''}{duration if duration != str(ppz8_config['default_length']) else ''} "

        channel_data.append(output)

    return num_filled, channel_data, data_tracker

# master function for parsing measures
def parse_parts(drum_parts, w_file, flag_unknown_instruments, ppz8_config):

    # reduce all drum parts to just measures
    drum_parts = {
        "music21": [list(part) for part in drum_parts["music21"]],
        "xml": [part.findall("measure") for part in drum_parts["xml"]]
    }

    num_channels_used = 0

    m_library = [[],[],[],[],[],[],[],[]]
    data_tracker = {'detune': None, 'octave': None, 'panning': None}
    data_tracker = [deepcopy(data_tracker) for i in range(8)]
    f = open(w_file, "w", encoding="utf-8")
    # fundamental assumption: all parts have equal measures
    for m_idx in range(len(drum_parts["music21"][0])):

        if m_idx in [elem[0]-1 for elem in ppz8_config["break_measures"]]:
            data_tracker = {'detune': None, 'octave': None, 'panning': None}
            data_tracker = [deepcopy(data_tracker) for i in range(8)]

        # extract a single column of measures, and then generate pattern for it
        measures = {
            "music21": [part_m[m_idx] for part_m in drum_parts["music21"]],
            "xml": [part_m[m_idx] for part_m in drum_parts["xml"]]
        }

        measure_channel_count, pmd_code, data_tracker = new_pattern(measures, flag_unknown_instruments, ppz8_config, data_tracker)
        num_channels_used = max(num_channels_used, measure_channel_count)

        for idx in range(len(m_library)):
            m_library[idx].append(pmd_code[idx])

    m_library = m_library[:num_channels_used]
    for measure_group in ppz8_config["break_measures"]:
        print(f"\n; Measures {measure_group[0]} to {measure_group[1]}", file=f)
        for channel_idx, channel in enumerate(m_library):
            print(f"{ppz8_config['ppz8_channelnames'][channel_idx]} {''.join(channel[measure_group[0]-1:measure_group[1]])}", file=f)

if __name__ == "__main__":

    # construct parsing objects
    parser = argparse.ArgumentParser(
        description="Automatic generation of PMD percussion code using MIDI source files.")
    parser.add_argument(
        "input_file", type=str,
        help="Must be a MIDI or MuseScore file")
    parser.add_argument(
        "ppz8_config", type=str,
        help="Config file for setting ppz8 drum definitions"
    )
    parser.add_argument(
        "-flag_inst", "--flag_unknown_instruments",
        action="store_true", default=False,
        help="Flag unrecognized drum instruments via Exception")
    parser.add_argument(
        "-o", "--output", type=str, default=None,
        help="Specify relative path to custom output file for PMD code")
    parser.add_argument(
        "-k", "--keep-mxl-file", action="store_true", default=False,
        help="Keep converted .xml file after program is done"
    )
    args = parser.parse_args()

    config = json.load(open(args.ppz8_config, encoding="utf-8"))

    # do file validation
    f_split = os.path.splitext(args.input_file)
    if f_split[1] in [".mid", ".midi", ".mscz"]:
        print("Converting MIDI/MuseScore input to MusicXML...")
        subprocess.run(
            ["mscore", "-o", f_split[0] + ".mxl", args.input_file],
            stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL, check=False
        )
    if not os.path.isfile(f_split[0]+".mxl"):
        raise AttributeError("An invalid music file type was submitted!")

    # proceed once file validation completed
    drum_parts = check_for_drums(f_split[0]+".mxl")
    if len(drum_parts) == 0:
        raise AttributeError("No drum parts were found in your file.")

    # construct default output file if needed, then pass to next method
    if args.output is None:
        args.output = f_split[0] + ".txt"
    print("Now writing to output " + args.output)
    parse_parts(drum_parts,
                os.path.join(os.path.dirname(__file__), args.output),
                args.flag_unknown_instruments,
                config)
    if not args.keep_mxl_file:
        print("Removing generated mxl file")
        os.remove(f_split[0]+".mxl")
