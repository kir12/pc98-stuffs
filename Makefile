SONGNAME = markets2
make:
	python msdos.py revival.mml
clean:
	rm -f PMD/songs/*.m
	rm -f PMD/songs/*.m2
	rm -f *.flac
	rm -f *.wav
fmpmd:
	wine PMD/fmpmd/FMPMDE.exe &
test:
	python xml_anal.py library/fukashigi_no_carte/Fukashigi_no_Carte_-_Rascal_Does_Not_Dream_of_Bunny_Girl_Senpai_ED_[FULL].mscz
pmd:
	python merge_voices.py -i library/heaven/heaven.mscz -idx 13 -r 65 80  parse-pmd -k be -d 16 -c 2
makewindows:
	python msdos.py $(SONGNAME).mml
	cp PMD/songs/$(SONGNAME).m2 /mnt/c/Users/leebri/Desktop/pc98-stuffs/PMD/songs/$(SONGNAME).m2
