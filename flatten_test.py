from music21 import converter
from music21.chord import Chord
from music21.note import Note, Rest
from music21.stream import Stream
from copy import deepcopy

if __name__ == "__main__":
    score = converter.parse("/home/brianl/Downloads/dullahanunderthewillowsmidi.mid")
    chor = list(score.parts[2].chordify().flatten().notesAndRests)
    cpy_chor = deepcopy(chor)
    for idx, elem in enumerate(cpy_chor):
        if type(elem) != Chord or Note:
            continue
        elif type(elem) == Note and elem.tie.type in ['stop', 'continue']:
            chor[idx] = Note(duration = elem.duration, offset = elem.offset) 
        elif type(elem) == Chord:
            newchord = deepcopy(list(elem.notes))
            newchord = [elem for elem in newchord if elem.tie.type == 'start']
            chor[idx].notes = newchord

    a = Stream(chor).write("midi", fp="test.midi")
    # notesequence = score.parts[3].flatten()
    # notesequence = [elem for elem in notesequence if type(notesequence) in [Chord, Note, Rest]]
    # notesequence_iter = deepcopy(notesequence)

