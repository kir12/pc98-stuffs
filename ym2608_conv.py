import argparse
from pathlib import Path

SSG_DRUMS = [1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024]

YM2608 = {
    1: "\b",
    2: 
}

def deconstruct_drums(instnum):
    """Convert SSG drum inst to equivalent YM2608 insts."""
    inst_arr = []
    inst_track = instnum
    for inst in reversed(SSG_DRUMS):
        if inst_track < inst:
            continue
        elif inst_track == 0:
            break
        else:
            inst_track -= inst
            inst_arr.append(inst)

    return inst_arr



def process_line(drumline):
    """Convert SSG drum pattern to YM2608 pattern."""
    # split into parts
    old_drum_notes = drumline.split(" ")
    drum_notes = old_drum_notes[0]

    for elem in old_drum_notes[1:-1]:
        if elem.startswith('@'):
            instnum, duration = elem.split("c")
            instnum = int(instnum[1:])
            ym2608_arr = deconstruct_drums(instnum)
            drum_notes.append(f"{ym2608_arr}r{duration}")
        else:  # rests
            drum_notes.append(elem)

    drum_notes.append(old_drum_notes[-1])

    return drum_notes


if __name__ == "__main__":
    # parse args
    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('path', type=str, help='path to drum file.')
    args = parser.parse_args()

    with open(args.path, 'r', encoding='UTF-8') as drum_fle:
        parentlib = Path(args.path).parent
        with open(str(parentlib/'ym2608_drums.txt'), 'w', encoding='UTF-8') as w_f:
            for line in drum_fle:
                # ignore comments
                if line.startswith(';'):
                    continue
                
                drum_array = process_line(line)

                w_f.write(" ".join(drum_array))
